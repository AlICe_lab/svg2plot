<!--
SPDX-FileCopyrightText: 2023 julien rippinger

SPDX-License-Identifier: CC0-1.0
-->

# Project Status: :rotating_light: Merged & Archived :rotating_light:

This project has been merged with [codeberg.org/mononym/chimera](https://codeberg.org/mononym/chimera) where its development continues.