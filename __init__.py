# SPDX-FileCopyrightText: 2023 julien rippinger
#
# SPDX-License-Identifier: MIT

import os
import sys
from time import sleep
from tempfile import NamedTemporaryFile

from serial import Serial
from tomli_w import dumps
from vpype_cli import execute
from vpype import read_multilayer_svg
from serial import Serial
from rich import box
from rich.console import Console
from rich.columns import Columns
from rich.table import Table
from rich.prompt import Prompt, Confirm, IntPrompt
from rich.panel import Panel
from rich.highlighter import RegexHighlighter
from rich.theme import Theme
from rich.align import Align
from rich.progress import (
    BarColumn,
    MofNCompleteColumn,
    Progress,
    TextColumn,
    TimeElapsedColumn,
)


class Controller(Serial):
    """Class to communicate with the plotter's controller."""

    def __init__(self, path="/dev/ttyUSB0", baudrate=115200):
        Serial.__init__(self, path, baudrate)

    def send(self, commands):
        # convert single command to list
        commands = [commands] if isinstance(commands, str) else commands
        # send commands
        for line in commands:
            data = line.strip()
            self.write((data + "\n").encode())  # Send g-code block to grbl
            # Wait for grbl response with carriage return
            grbl_out = self.readline().strip().decode()
            console.print("\[" + data + "]:[RTN:" + grbl_out + "]")

        return grbl_out

    def stream(self, commands):
        # convert single command to list
        commands = [commands] if isinstance(commands, str) else commands
        # Define custom progress bar
        progress_bar = Progress(
            TextColumn("[progress.percentage]{task.percentage:>3.0f}%"),
            BarColumn(bar_width=None),
            MofNCompleteColumn(),
            TimeElapsedColumn(),
            console=console,
            expand=True,
        )

        # Use custom progress bar
        with progress_bar as p:
            for line in p.track(commands):
                # send command
                self.send(line)

    def connect(self):
        tasks = ["connection to plotter"]

        with console.status("[yellow]connecting...") as status:
            while tasks:
                task = tasks.pop(0)
                # Wake up grbl
                self.write("\r\n\r\n".encode())
                sleep(2)  # Wait for grbl to initialize
                self.flushInput()  # Flush startup text in serial input
                console.print(
                    Panel(
                        f":zap: {task} established\nlong text explaining how to setup the plotter",
                        title="[magenta]setup plotter",
                        box=box.ASCII,
                    )
                )

    def disconnect(self):
        tasks = ["disconnecting plotter"]

        with console.status("[yellow]disconnecting...") as status:
            while tasks:
                task = tasks.pop(0)
                # Wake up grbl
                self.close()
                console.print(Panel(f":zap: plotter disconnected", box=box.ASCII))


class GcodeHighlighter(RegexHighlighter):
    """Apply highlight style to anything that looks like gcode."""

    base_style = "gcode."
    highlights = [
        r"(?P<g>([Gg][0-9][0-9]))",
        r"(?P<m2>(M2))",
        r"(?P<setting>(\$[A-Z]))",
        r"(?P<speed>(F\d+.0))",
        r"(?P<ok>(ok))",
        r"(?P<close>(\]))",
        r"(?P<in>(\[))",
        r"(?P<out>(:\[RTN:))",
        r"(?P<error>(error:[0-9]))",
        r"(?P<end>(MSG:Pgm End))",
        r"(?P<comment>(\(([^]]+)\)))",
        r"(?P<x>(([Xx]) *(-?\d+.?\d*)))",
        r"(?P<y>(([Yy]) *(-?\d+.?\d*)))",
        r"(?P<z>(([Zz]) *([-+]?\d+.?\d*)))",
    ]


def get_files_in_directory(directory):
    # Function to get the files in a directory
    # Initialize table
    file_table = Table(show_header=True, expand=True, box=box.ASCII2, header_style="")
    file_table.add_column("#", ratio=3, style="cyan", justify="right")
    file_table.add_column(directory + "/", ratio=21, style="green")
    file_table.add_column("type", ratio=13, style="yellow")
    # file_table.add_column("Path", style="magenta")

    # Add files to the table
    item_dict = {}

    # Collect files and directories
    file_items = []
    dir_items = []

    if directory != os.getenv("HOME"):
        # Add parent direcory
        file_table.add_row(f"[yellow]0", f"[yellow]BACK")
        item_dict[0] = os.path.abspath(os.path.join(directory, os.pardir))

    for item in sorted(os.listdir(directory)):
        if item.startswith("."):
            continue
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            item = f"[blue]{item.upper()}"
            dir_items.append((item, item_path))
        if os.path.isfile(item_path):
            if item.endswith(".gcode") or item.endswith(".svg"):
                file_items.append((item, item_path))

    for index, item in enumerate(dir_items + file_items):
        item_type = "folder" if item in dir_items else "file"
        file_table.add_row(str(index + 1), item[0], item_type)
        item_dict[index + 1] = item[1]

    return file_table, item_dict


def header():
    """
    TODO: add some parameters (/dev/ttyUSB0, baudrate, etc.)
    """
    title_text = """
1. Select folder and files to plot
2. Convert SVG to GCODE
3. Setup plotter
4. Send GCODE to plotter
5. Repeat
"""
    # Flush terminal
    os.system("cls" if os.name == "nt" else "clear")
    # Display header
    console.print(
        Panel(title_text, title="[magenta]<svg>-2-<plotter>", box=box.HORIZONTALS)
    )


def file_browser(current_directory):
    file_prompt_text = "[yellow]#[/yellow] [cyan]index[/cyan] of the [blue]directory[/blue] or [green]file[/green] you want to navigate/select.\n[yellow]0[/yellow] to return to previous directory.\n[yellow]D[/yellow] key to return to desktop.\n[yellow]Q[/yellow] key to quit."

    # Display files in the current directory
    console.print(
        Panel(
            file_prompt_text,
            title="[magenta]file browsing commands",
            subtitle="[magenta]file browser",
            box=box.ASCII,
        )
    )

    file_table, file_dict = get_files_in_directory(current_directory)
    console.print(file_table)

    # Loop for selecting a subdirectory
    while True:
        # Prompt for user input

        selected_index = Prompt.ask("[yellow]prompt")
        # Check for quit command
        if selected_index.lower() == "q":
            sys.exit(1)

        # Find the selected directory path
        elif selected_index.lower() == "d":
            selected_path = os.getenv("HOME") + "/Desktop"

        else:
            try:
                selected_path = file_dict.get(int(selected_index))
            except:
                console.print("[red]invalid index selected.")
                continue

            if not selected_path:
                console.print("[red]invalid index selected.")
                continue

        # If a directory was selected, display its files
        if os.path.isdir(selected_path):
            header()

            console.print(
                Panel(
                    file_prompt_text,
                    title="[magenta]file browsing commands",
                    box=box.ASCII,
                    subtitle="[magenta]file browser",
                )
            )

            file_table, file_dict = get_files_in_directory(selected_path)
            console.print(file_table)
        else:
            console.print(
                Panel(
                    "[green]{}".format(os.path.split(selected_path)[1]),
                    title="[magenta]selected file",
                    box=box.ASCII,
                )
            )
            if Confirm.ask("[yellow]confirm ?", default=True):
                break
            else:
                continue

    return selected_path


def no_size(svg):
    """
    Check in header if svg file has a width/height value.
    Adobe Illustrator tends not to.
    """
    check = False
    with open(svg, "r") as f:
        for line in f.readlines():
            if line.startswith("<svg"):
                line = line.split(">")[0]  # get single line svg
                check = False if "width=" in line or "height=" in line else True
                break
    return check


def is_adobe_svg(file, head=10):
    """
    Check in header if svg file has been generated by Adobe Illustrator.
    """
    check = False
    # Check if svg is generated from Adobe Illustrator
    with open(file, "r") as f:
        # parse first 10 lines
        for line in f.readlines()[0:head]:
            if "Adobe" in line or "Illustrator" in line:
                check = True
                break
    return check


def select_plot_pression():
    """
    Prompt selection of pressure value.
    """
    pressures = ["{:.1f}".format(i * 0.1) for i in range(1, 31)] + [
        str(i) for i in range(1, 4)
    ]

    selection = Prompt.ask(
        "[yellow]define pen pressure [bold magenta]\[min:0.1mm/max:3.0mm]",
        choices=pressures,
        show_choices=False,
        default="0.7",
    )

    return selection


def create_temporary_file(data, suffix=""):
    """Creates a tempfile.NamedTemporaryFile object for data
    source:https://programtalk.com/vs4/python/Aguila-team/aguila_nlu/rasa_nlu/utils/__init__.py/
    """
    f = NamedTemporaryFile("w+", suffix=suffix, delete=False, encoding="utf-8")
    f.write(data)
    f.close()

    return f.name


def make_toml(p):
    # convert input to relative z value
    p = float(p)
    z_value = f"{(5. + p):.4f}"

    # insert z_value into custom gcode profile
    toml_dict = {
        "gwrite": {
            "chelonograph": {
                "document_start": "G92 X0 Y0 Z0 (set origin)\nG21 (unit is mm)\nG17 (work in XY plane)\nG91 Z-5.0000 (pen up)\n",
                "segment_first": f"G90 X{{x:.4f}} Y{{y:.4f}} (travel)\nG91 Z+{z_value} (pen down)\nG90 (absolute position)\n",
                "segment": f"G01 X{{x:.4f}} Y{{y:.4f}} (draw)\n",
                "segment_last": f"G01 X{{x:.4f}} Y{{y:.4f}} (draw)\nG91 Z-{z_value} (pen up)\n",
                "document_end": "G90 X0 Y0 (travel)\nM2 (end)\n",
                "unit": "mm",
            }
        }
    }

    tmp_file = create_temporary_file(dumps(toml_dict), suffix=".toml")

    return tmp_file


def convert_svg_gcode(svg_file):
    # Load svg as vpype documnt
    drawing = read_multilayer_svg(svg_file, 0.4)

    # TODO: I do not remember why this is here
    # if no_size(svg_file):
    #     console.print("no width/heigth")

    # Check scale of svg file
    if is_adobe_svg(svg_file):
        # propose to change scale
        console.print(
            Panel(
                ":astonished: seems like the SVG has been saved with illustrator which bluntly ignores SVG conventions: your drawing is 25% too small.\n",
                title="WARNING",
                style="bright_red",
                box=box.DOUBLE,
            )
        )

        if Confirm.ask(
            "[yellow]do you want to scale the document to 96 ppi?", default=True
        ):
            svg_file = "{}_scaled.svg".format(svg_file[:-4])
            drawing = execute("scale 1.333 1.333 write {}".format(svg_file), drawing)

            console.print(
                Panel(
                    f"[green]{os.path.split(svg_file)[1]}",
                    title="[magenta]output",
                    box=box.ASCII,
                )
            )

    """
    2. Convert svg to gcode with vpype
    """
    # Optional: optimize geometry and paths
    if "_optimized" not in svg_file and Confirm.ask(
        "[yellow]optimize SVG before converting to gcode?", default=True
    ):
        # create new svg file
        svg_file = "{}_optimized.svg".format(svg_file[:-4])
        drawing = execute(
            "linesimplify reloop linemerge linesort write {}".format(svg_file), drawing
        )

    # get pression i.e. gwrite profile
    pression = select_plot_pression()

    gcode_file = "{}_p{:<02}.gcode".format(svg_file[:-4], pression.replace(".", ""))

    # make vpype gcode profile for custom pressure
    tmp_file = make_toml(pression)

    # convert svg to gcode
    drawing = execute(
        "gwrite -p {} {}".format("chelonograph", gcode_file),
        document=drawing,
        global_opt=f"--config {tmp_file}",
    )
    console.print(
        Panel(
            "[green]{}".format(os.path.split(gcode_file)[1]),
            title="[magenta]output",
            box=box.ASCII,
        )
    )

    # Show file before print

    if Confirm.ask("[yellow]preview drawing file?", default=False):
        # with suppress_stdout():
        # show(drawing)
        execute("show --classic --show-pen-up --colorful", document=drawing)

    return gcode_file


def setup_plotter():
    plotter = Controller("/dev/ttyUSB0", 115200)
    # plotter.set_rich_output(console)
    plotter.connect()

    plotter_lock = True
    while plotter_lock:
        if Confirm.ask("[yellow]unlock with homing cycle?", default=True):
            tasks = ["homing"]
            with console.status("[yellow]homing initiated...") as status:
                while tasks:
                    task = tasks.pop(0)
                    homing = plotter.send("$H")

            if homing != "ok":
                console.print(
                    Panel(
                        "some text saying FAIL\nCheck if the controller is powered?\nTry again choosing the previously generated gcode file.",
                        title="WARNING",
                        style="bright_red",
                        box=box.DOUBLE,
                    )
                )
            else:
                # console.log(f"[yellow]{task} complete")
                break

        elif Confirm.ask("[yellow]unlock manually?", default=True):
            plotter.send("$X")
            break

        else:
            console.print(
                Panel(
                    "plotter is still locked",
                    title="INFO",
                    style="bright_red",
                    box=box.DOUBLE,
                )
            )
            continue

    # choose plotting speed
    speed = IntPrompt.ask(
        "[yellow]move pen to origin ([bright_red]0[/bright_red],[bright_green]0[/bright_green],[bright_blue]0[/bright_blue]) and enter speed value [bold magenta]\[min:1%/max:100%]",
        default="75",
        choices=[str(n) for n in range(1, 101)],
        show_choices=False,
    )

    plotter.send("F{}".format(25000 * (int(speed) / 100)))

    # send file

    console.print(
        Panel(
            ":zap: hitting enter will send [green]{}[/green] to the plotter.".format(
                os.path.split(gcode_file)[1]
            ),
            title="[magenta]plotter launch pad",
            box=box.ASCII,
        )
    )

    if Confirm.ask("[yellow]start plotting?", default=True):
        # Stream g-code to grbl
        with open(gcode_file, "r") as f:
            plotter.stream(f.readlines())

        console.print(Panel("SUCCESS! :smiley: looks like it worked.", box=box.ASCII))

    else:
        # console.print('ABORT')
        console.print(
            Panel(
                "operation aborted. tip: return to file browser and select the previously generated GCODE file [green]{}".format(
                    os.path.split(gcode_file)[1]
                ),
                title="INFO",
                style="bright_red",
                box=box.DOUBLE,
            )
        )

    plotter.disconnect()


def main():
    current_directory = os.getenv("HOME") + "/Desktop"

    # Initialize console
    console = Console(
        highlighter=GcodeHighlighter(),
        theme=Theme(
            {
                "gcode.g": "bright_yellow",
                "gcode.m2": "bright_yellow",
                "gcode.setting": "bright_yellow",
                "gcode.speed": "bright_yellow",
                "gcode.ok": "bright_black",
                "gcode.close": "deep_pink4",
                "gcode.in": "deep_pink4",
                "gcode.out": "deep_pink4",
                "gcode.error": "blink",
                "gcode.end": "bright_black",
                "gcode.comment": "light_slate_grey",
                "gcode.x": "bright_red",
                "gcode.y": "bright_green",
                "gcode.z": "bright_blue",
            }
        ),
    )

    while True:
        # Make heading
        header()

        # Choose a file
        file = file_browser(current_directory)

        # Check if selection is SVG or GCODE
        svg_file = file if file.endswith(".svg") else False
        gcode_file = file if file.endswith(".gcode") else False

        # Convert SVG to GCODE
        if svg_file:
            gcode_file = convert_svg_gcode(svg_file)

        # Setup plotter
        setup_plotter()

        if Confirm.ask(
            "[yellow]return to file browser to plot another file?", default=True
        ):
            current_directory = os.path.split(file)[0]
            os.system("cls" if os.name == "nt" else "clear")
            continue
        else:
            # exit program
            break


if __name__ == "__main__":
    main()
